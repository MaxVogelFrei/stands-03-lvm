# Домашнее задание 3

лог работы [script](typescript)

## Работа с файловыми системами и LVM

* уменьшить том под / до 8G
* выделить том под /home
* выделить том под /var
* /var - сделать в mirror
* /home - сделать том для снэпшотов
*	- сгенерить файлы в /home/
*	- снять снэпшот
*	- удалить часть файлов
*	- восстановится со снэпшота
* прописать монтирование в fstab

* залоггировать работу с помощью утилиты script

## Процесс решения

	исходное состояние
		[root@lvm ~]# lsblk
		NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
		sda                       8:0    0   40G  0 disk
		├─sda1                    8:1    0    1M  0 part
		├─sda2                    8:2    0    1G  0 part /boot
		└─sda3                    8:3    0   39G  0 part
		├─VolGroup00-LogVol00 253:0    0 37.5G  0 lvm  /
		└─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
		sdb                       8:16   0   10G  0 disk
		sdc                       8:32   0    2G  0 disk
		sdd                       8:48   0    1G  0 disk
		sde                       8:64   0    1G  0 disk

### Уменьшение /
	Потребуется утилита xfsdump
		[root@lvm ~]# yum -y install xfsdump

	Под временный корень создаю физ. том, группу, лог. том, ФС, монтирую в /mnt
		[root@lvm ~]# pvcreate /dev/sdb
		[root@lvm ~]# vgcreate vg_root /dev/sdb
		[root@lvm ~]# lvcreate -n lv_root -l+100%FREE /dev/vg_root
		[root@lvm ~]# mkfs.xfs /dev/vg_root/lv_root
		[root@lvm ~]# mount /dev/vg_root/lv_root /mnt

	Копирую содержимое / в новый том
		[root@lvm ~]# xfsdump -J - /dev/VolGroup00/LogVol00 | xfsrestore -J - /mnt

	Монтирую виртуальные ФС в новом корне
		[root@lvm ~]# for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done

	chroot в новый корень и обновление конфига загрузчика
		[root@lvm ~]# chroot /mnt/
		[root@lvm /]# grub2-mkconfig -o /boot/grub2/grub.cfg
		[root@lvm /]# cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done

	в grub.cfg 
		[root@lvm /]# vi /boot/grub2/grub.cfg
	в строке
		linux16 /vmlinuz-3.10.0-862.2.3.el7.x86_64 root=
	меняю rd.lvm.lv=VolGroup00/LogVol00 на rd.lvm.lv=vg_root/lv_root

	после перезагрузки вижу что / в новом томе
		[root@lvm ~]# reboot
		[root@lvm ~]# lsblk
		NAME                    MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
		sda                       8:0    0   40G  0 disk
		├─sda1                    8:1    0    1M  0 part
		├─sda2                    8:2    0    1G  0 part /boot
		└─sda3                    8:3    0   39G  0 part
		├─VolGroup00-LogVol01 253:1    0  1.5G  0 lvm  [SWAP]
		└─VolGroup00-LogVol00 253:2    0 37.5G  0 lvm
		sdb                       8:16   0   10G  0 disk
		└─vg_root-lv_root       253:0    0   10G  0 lvm  /
		sdc                       8:32   0    2G  0 disk
		sdd                       8:48   0    1G  0 disk
		sde                       8:64   0    1G  0 disk
	теперь можно удалить старый	
		[root@lvm ~]# lvremove /dev/VolGroup00/LogVol00
	создаю том и фс под / меньшего размера
		[root@lvm ~]# lvcreate -n VolGroup00/LogVol00 -L 8G /dev/VolGroup00
		[root@lvm ~]# mkfs.xfs /dev/VolGroup00/LogVol00
		[root@lvm ~]# mount /dev/VolGroup00/LogVol00 /mnt
	копирую данные из временного корня в уменьшенный
		[root@lvm ~]# xfsdump -J - /dev/vg_root/lv_root	| xfsrestore -J - /mnt
	монтирую виртуальные фс, chroot в новый корень, обновление загрузчика
		[root@lvm ~]# for i in /proc/ /sys/ /dev/ /run/ /boot/; do mount --bind $i /mnt/$i; done
		[root@lvm ~]# crhroot /mnt/
		[root@lvm /]# grub2-mkconfig -o /boot/grub2/grub.cfg
		[root@lvm /]# cd /boot ; for i in `ls initramfs-*img`; do dracut -v $i `echo $i|sed "s/initramfs-//g; s/.img//g"` --force; done

### Перенос /var на зеркало LVM
	Создаю физ.тома на 2 устройствах, группу на них и лог. том с флагом m1 для создания зеркала
		[root@lvm boot]# pvcreate /dev/sdc /dev/sdd
		[root@lvm boot]# vgcreate vg_var /dev/sdc /dev/sdd
		[root@lvm boot]# lvcreate -L 950M -m1 -n lv_var vg_var
		[root@lvm boot]# mkfs.ext4  /dev/vg_var/lv_var
	Монтирую в /mnt и rsync'ом переношу данные	
		[root@lvm boot]# mount /dev/vg_var/lv_var /mnt
		[root@lvm boot]# rsync -avHPSAX /var/ /mnt/
	Во временную папку переношу содержимое старого раздела
		[root@lvm boot]# mkdir /tmp/oldvar && mv /var/* /tmp/oldvar
	Перемонтирую том в /var, добавляю строку для автомонтирования в fstab
		[root@lvm boot]# umount /mnt
		[root@lvm boot]# mount /dev/vg_var/lv_var /var
		[root@lvm boot]# echo "`blkid | grep var: | awk '{print $2}'` /var ext4 defaults 0 0" >> /etc/fstab
		[root@lvm boot]# exit
		[root@lvm ~]# reboot
	После перезагрузки вижу что / и /var правильно подключены на новых томах
		[root@lvm ~]# lsblk
		NAME                     MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
		sda                        8:0    0   40G  0 disk
		├─sda1                     8:1    0    1M  0 part
		├─sda2                     8:2    0    1G  0 part /boot
		└─sda3                     8:3    0   39G  0 part
  		├─VolGroup00-LogVol00  253:0    0    8G  0 lvm  /
  		└─VolGroup00-LogVol01  253:1    0  1.5G  0 lvm  [SWAP]
		sdb                        8:16   0   10G  0 disk
		└─vg_root-lv_root        253:2    0   10G  0 lvm
		sdc                        8:32   0    2G  0 disk
		├─vg_var-lv_var_rmeta_0  253:3    0    4M  0 lvm
		│ └─vg_var-lv_var        253:7    0  952M  0 lvm  /var
		└─vg_var-lv_var_rimage_0 253:4    0  952M  0 lvm
  		└─vg_var-lv_var        253:7    0  952M  0 lvm  /var
		sdd                        8:48   0    1G  0 disk
		├─vg_var-lv_var_rmeta_1  253:5    0    4M  0 lvm
		│ └─vg_var-lv_var        253:7    0  952M  0 lvm  /var
		└─vg_var-lv_var_rimage_1 253:6    0  952M  0 lvm
  		└─vg_var-lv_var        253:7    0  952M  0 lvm  /var
		sde                        8:64   0    1G  0 disk
	Удаляю ненужные тома из lvm
		[root@lvm ~]# lvremove /dev/vg_root/lv_root
		[root@lvm ~]# vgremove /dev/vg_root
		[root@lvm ~]# pvremove /dev/sdb

### Перенос /home на том со снапшотами
	Создаю лог. том, ФС под /home
		[root@lvm ~]# lvcreate -n LogVol_Home -L 2G /dev/VolGroup00
		[root@lvm ~]# mkfs.xfs /dev/VolGroup/LogVol_Home
	Монтирую в /mnt и копирую данные из старого /home, удаляю файлы по старому пути
		[root@lvm ~]# mount /dev/VolGroup00/LogVol_Home /mnt/
		[root@lvm ~]# cp -aR /home/* /mnt/
		[root@lvm ~]# rm -rf /home/*
	Перемонтирую том из /mnt в /home, добавляю строку в fstab
		[root@lvm ~]# umount /mnt
		[root@lvm ~]# mount /dev/VolGroup00/LogVol_Home /home/
		[root@lvm ~]# echo "`blkid | grep Home | awk '{print $2}'` /home xfs defaults 0 0" >> /etc/fstab
	Создаю файлы для проверки работы снапшота
		[root@lvm ~]# touch /home/file{1..25}
	Создаю том для хранения снапшотов
		[root@lvm ~]# lvcreate -L 100MB -s -n home_snap /dev/VolGroup00/LogVol_Home
	Удаляю часть файлов	
		[root@lvm ~]# rm -f /home/file{9..23}
	Размонтирую /home и восстанавливаю состояние тома из снапшота, монтирую обратно
		[root@lvm ~]# umount /home
		[root@lvm ~]# lvconvert --merge /dev/VolGroup00/home_snap
		[root@lvm ~]# mount /home
	Вижу что файлы восстановлены	
		[root@lvm ~]# ls -la /home
		total 4
		drwxr-xr-x.  3 root    root    4096 Nov 18 10:01 .
		drwxr-xr-x. 18 root    root     239 Nov 18 09:44 ..
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file1
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file10
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file11
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file12
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file13
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file14
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file15
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file16
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file17
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file18
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file19
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file2
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file20
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file21
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file22
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file23
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file24
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file25
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file3
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file4
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file5
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file6
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file7
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file8
		-rw-r--r--.  1 root    root       0 Nov 18 10:01 file9
		drwx------.  3 vagrant vagrant   95 Nov 18 10:03 vagrant
